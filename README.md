# thumbtack
A Redis-like simple database.


GUIDENCE:

1. This database has no 3rd-party dependencies.
2. Run program by

  ./run.py

  (Add 'chmod 777 run.py' if not executable.)

3. Program starts with a mode selection:

  mode 1 -> input in cmd line mode.
  mode 2 -> input from a 'input.txt' file.

4. Written in Vim, does not require specific IDE.
5. OS : Mac OS X 10.10.1

5. Files:

  run.py : Execute this file to run database.
  db.py  : Class of database.
  input.txt : input commands in mode 2.
  output.txt : output results in mode 2.

6. This database only supports 'key - value' pair, where key is a string, value is an integer.
   Certain change will be needed to support other formats.


OTHER INFO:

1. This database takes O(1) time to all operations including GET, SET, UNSET AND NUMEQUALTO,
   both worst-case and best-case.

2. Main mem is a LRU.

3. ROLLBACK is implemented by a stack.

4. Regarding vast majority of transactions operated on a small number of variables, the space
   complexity is O(N), while N is the total number of variables.
